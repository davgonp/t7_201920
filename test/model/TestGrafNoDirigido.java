package model;

import static org.junit.Assert.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import model.data_structures.*;

import org.junit.Test;

public class TestGrafNoDirigido {

	private GrafNoDirigido<String, String> grafoTest;

	private ArrayList<Vertice<String, String>> datos;

	@Before
	public void setupEscenario1(){
		grafoTest = new GrafNoDirigido<String, String>();
	}

	@Before
	public void setupEscenario2(){
		grafoTest = new GrafNoDirigido<String, String>();

		datos = new ArrayList<Vertice<String, String>>();
		datos.add(new Vertice<String, String>("0", "Kira"));
		datos.add(new Vertice<String, String>("1", "Juan"));
		datos.add(new Vertice<String, String>("2", "Luis"));
		datos.add(new Vertice<String, String>("3", "Pedro"));
		datos.add(new Vertice<String, String>("4", "Jose"));

		grafoTest.addVertex(datos.get(0).darIdVertice(), datos.get(0).darInfoVer());
		grafoTest.addVertex(datos.get(1).darIdVertice(), datos.get(1).darInfoVer());
		grafoTest.addVertex(datos.get(2).darIdVertice(), datos.get(2).darInfoVer());
		grafoTest.addVertex(datos.get(3).darIdVertice(), datos.get(3).darInfoVer());
		grafoTest.addVertex(datos.get(4).darIdVertice(), datos.get(4).darInfoVer());
	}

	@Test
	public void VTest(){
		setupEscenario1();
		assertEquals("Deber�a ser cero", 0, grafoTest.V());

		setupEscenario2();
		assertEquals("Deber�a ser cinco", 5, grafoTest.V());
	}

	@Test
	public void ETest(){
		setupEscenario1();
		assertEquals("Deber�a ser cero", 0, grafoTest.E());

		setupEscenario2();
		assertEquals("Deber�a ser cero", 0, grafoTest.E());
	}

	@Test
	public void addVertexTest(){
		setupEscenario2();
		assertEquals("Deber�a ser cinco", 5, grafoTest.V());

		datos.add(new Vertice<String, String>("5", "Lucas"));
		datos.add(new Vertice<String, String>("6", "Natalia"));
		datos.add(new Vertice<String, String>("7", "Fabio"));
		datos.add(new Vertice<String, String>("8", "Carolina"));
		datos.add(new Vertice<String, String>("9", "Hernando"));

		grafoTest.addVertex(datos.get(5).darIdVertice(), datos.get(5).darInfoVer());
		grafoTest.addVertex(datos.get(6).darIdVertice(), datos.get(6).darInfoVer());
		grafoTest.addVertex(datos.get(7).darIdVertice(), datos.get(7).darInfoVer());
		grafoTest.addVertex(datos.get(8).darIdVertice(), datos.get(8).darInfoVer());
		grafoTest.addVertex(datos.get(9).darIdVertice(), datos.get(9).darInfoVer());

		assertEquals("Deber�a ser cinco", 10, grafoTest.V());
	}

	@Test
	public void getInfoVertexTest(){
		setupEscenario1();
		assertNull(grafoTest.getInfoVertex("0"));

		setupEscenario2();
		int contador = 0;
		for (Vertice<String, String> vertice : datos) {
			assertEquals("No es el dato esperado", vertice.darInfoVer(), grafoTest.getInfoVertex(contador + ""));
			contador++;
		}
	}

	@Test
	public void setInfoVertexTest(){
		setupEscenario1();
		grafoTest.setInfoVertex("0", "Lisa");
		assertNull(grafoTest.getInfoVertex("0"));


		setupEscenario2();
		getInfoVertexTest();

		datos = new ArrayList<Vertice<String,String>>();
		datos.add(new Vertice<String, String>("0", "Lucas"));
		datos.add(new Vertice<String, String>("1", "Natalia"));
		datos.add(new Vertice<String, String>("2", "Fabio"));
		datos.add(new Vertice<String, String>("3", "Carolina"));
		datos.add(new Vertice<String, String>("4", "Hernando"));

		for (Vertice<String, String> vertice : datos) {
			grafoTest.setInfoVertex(vertice.darIdVertice(), vertice.darInfoVer());
		}

		int contador = 0;
		for (Vertice<String, String> vertice : datos) {
			assertEquals("No es el dato esperado", vertice.darInfoVer(), grafoTest.getInfoVertex(contador + ""));
			contador++;
		}
	}

	@Test
	public void addEdgeTest(){
		setupEscenario1();
		grafoTest.addEdge("0", "1", 3.1);
		assertEquals("Deber�a ser cero", 0, grafoTest.E());

		setupEscenario2();
		grafoTest.addEdge("0", "1", 4.4);
		grafoTest.addEdge("1", "2", 5.2);
		grafoTest.addEdge("2", "3", 7.1);
		grafoTest.addEdge("3", "4", 9.1);
		grafoTest.addEdge("1", "4", 1.1);
		grafoTest.addEdge("2", "4", 0.2);
		assertEquals("Deber�a ser cuatro", 6, grafoTest.E());
	}

	@Test
	public void getCostArcTest(){
		addEdgeTest();
		assertEquals("No es el dato esperado", 0.0 + "", grafoTest.getCostArc("0", "0") + "");

		assertEquals("No es el dato esperado", 4.4 + "", grafoTest.getCostArc("0", "1") + "");
		assertEquals("No es el dato esperado", 5.2+ "" ,grafoTest.getCostArc("1", "2")+ "");
		assertEquals("No es el dato esperado", 7.1+ "", grafoTest.getCostArc("2", "3")+ "");
		assertEquals("No es el dato esperado", 9.1+ "", grafoTest.getCostArc("3", "4")+ "");
		assertEquals("No es el dato esperado", 1.1+ "", grafoTest.getCostArc("1", "4")+ "");
		assertEquals("No es el dato esperado", 0.2+ "", grafoTest.getCostArc("2", "4")+ "");

		assertEquals("No es el dato esperado", 4.4+ "", grafoTest.getCostArc("1", "0")+ "");
		assertEquals("No es el dato esperado", 5.2+ "", grafoTest.getCostArc("2", "1")+ "");
		assertEquals("No es el dato esperado", 7.1+ "", grafoTest.getCostArc("3", "2")+ "");
		assertEquals("No es el dato esperado", 9.1+ "", grafoTest.getCostArc("4", "3")+ "");
		assertEquals("No es el dato esperado", 1.1+ "", grafoTest.getCostArc("4", "1")+ "");
		assertEquals("No es el dato esperado", 0.2+ "", grafoTest.getCostArc("4", "2")+ "");
	}

	@Test
	public void setInfoArcTest(){
		getCostArcTest();


		grafoTest.setCostArc("0", "1", 7.7);
		assertEquals("No es el dato esperado", 7.7+ "", grafoTest.getCostArc("0", "1")+ "");
		assertEquals("No es el dato esperado", 7.7+ "", grafoTest.getCostArc("1", "0")+ "");

		grafoTest.setCostArc("1", "2", 2.1);
		assertEquals("No es el dato esperado", 2.1+ "", grafoTest.getCostArc("1", "2")+ "");
		assertEquals("No es el dato esperado", 2.1+ "", grafoTest.getCostArc("2", "1")+ "");

		grafoTest.setCostArc("1", "4", 0.6);
		assertEquals("No es el dato esperado", 0.6+ "", grafoTest.getCostArc("1", "4")+ "");
		assertEquals("No es el dato esperado", 0.6+ "", grafoTest.getCostArc("4", "1")+ "");
	}

	@Test
	public void adjTest(){
		getCostArcTest();
		Iterator<String> it1 = grafoTest.adj("0");

		while(it1.hasNext()){
			assertEquals("No es el dato esperado", "1", it1.next());
		}

		Iterator<String> it2 = grafoTest.adj("1");

		int contador = 0;
		String[] adj1 = {"0", "2", "4"};
		while(it2.hasNext()){
			assertEquals("No es el dato esperado", adj1[contador], it2.next());
			contador++;
		}
	}

	@Test
	public void JSONTest(){
		setupEscenario2();
		grafoTest.addEdge(datos.get(1).darIdVertice(), datos.get(2).darIdVertice(), 32.1);
		grafoTest.addEdge(datos.get(4).darIdVertice(), datos.get(3).darIdVertice(), 73.1);
		grafoTest.addEdge(datos.get(4).darIdVertice(), datos.get(4).darIdVertice(), 98.6);

		try{
			FileWriter file = new FileWriter("./data/JSONPrueba.json");
			Gson gson = new Gson();
			gson.toJson(grafoTest, file);
			file.flush();
			file.close();
		} catch (Exception e1) {
			e1.printStackTrace();
			fail();
		}
	}

	@Test
	public void ccTest() {
		setupEscenario2();
		assertEquals("No es el dato esperado", 5, grafoTest.cc());
	}

	@Test
	public void getCCTest() {
		setupEscenario2();
		Iterable<String> que = grafoTest.getCC("0");
		int i = 0;
		for (String string : que) {
			i++;
		}
		assertEquals("No es el dato esperado", 0, i);
	}



}
