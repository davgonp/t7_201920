package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class GrafNoDirigido<K, V> /*implements IGraph*/{

	private K key;

	public SeparateChainingHash<K, Vertice<K, V>> tablaHashVertices;
	public SeparateChainingHash<K, Queue<Arco>> tablaHashArcos;

	private int vertices;
	private int arcos;


	public GrafNoDirigido(){
		vertices = 0;
		arcos = 0;
		tablaHashVertices = new SeparateChainingHash<K, Vertice<K,V>>(3);
		tablaHashArcos = new SeparateChainingHash<K, Queue<Arco>>(5);

	}

	public GrafNoDirigido(int pV){
		vertices = 0;
		arcos = 0;
		tablaHashVertices = new SeparateChainingHash<K, Vertice<K,V>>(pV);
		tablaHashArcos = new SeparateChainingHash<K, Queue<Arco>>(5);

	}

	public int V(){
		return vertices;
	}

	public int E(){
		return arcos;
	}

	public void addVertex(K idVertex, V infoVertex){
		Vertice<K, V> vertice = new Vertice<K, V>(idVertex, infoVertex);
		if(!tablaHashVertices.contains(idVertex)){
			vertices++;
		}
		tablaHashVertices.put(idVertex, vertice);
	}

	public V getInfoVertex(K idVertex){
		Object rta = tablaHashVertices.get(idVertex);
		if(rta != null) {
			return tablaHashVertices.get(idVertex).darInfoVer();
		}
		return null;
	}

	public void setInfoVertex(K idVertex, V infoVertex){
		Vertice<K, V> vertice = new Vertice<K, V>(idVertex, infoVertex);
		if(tablaHashVertices.contains(idVertex)){
			tablaHashVertices.put(idVertex, vertice);
		}
	}

	public boolean hasEdge(Vertice<K, V> pV1, Vertice<K, V> pV2) {
		if(pV1 != null && pV2 != null) {
			Queue<Arco> arcos = tablaHashArcos.get(pV1.darIdVertice());
			if(arcos != null) {
				for (Arco arco : arcos) {
					if((arco.darVer2().equals(pV2))){
						return true;
					}
				}	
			}
			Queue<Arco> arcos2 = tablaHashArcos.get(pV2.darIdVertice());
			if(arcos2 != null) {
				for (Arco arco : arcos2) {
					if((arco.darVer2().equals(pV2))){
						return true;
					}
				}	
			}
		}
		return false;
	}

	public void addEdge(K idVertexIni, K idVertexFin, double infoArc){
		Vertice<K, V> v1 = tablaHashVertices.get(idVertexIni);
		Vertice<K, V> v2 = tablaHashVertices.get(idVertexFin);
		if(v1 != null && v2 != null && !hasEdge(v1, v2)){
			if(tablaHashArcos.get(idVertexIni) == null) {
				Queue<Arco> arcosNuevos = new Queue<Arco>();
				arcosNuevos.enqueue(new Arco<K, V>(v1, v2, infoArc));
				tablaHashArcos.put(idVertexIni, arcosNuevos);
			}
			else {
				tablaHashArcos.get(idVertexIni).enqueue(new Arco<K, V>(v1, v2, infoArc));
			}
			if(tablaHashArcos.get(idVertexFin) == null) {
				Queue<Arco> arcosNuevos = new Queue<Arco>();
				arcosNuevos.enqueue(new Arco<K, V>(v2, v1, infoArc));
				tablaHashArcos.put(idVertexFin, arcosNuevos);
			}
			else {
				tablaHashArcos.get(idVertexFin).enqueue(new Arco<K, V>(v2, v1, infoArc));
			}
			arcos++;
		}
		else if(v1 != null && v2 != null && hasEdge(v1, v2)){
			setCostArc(idVertexIni, idVertexFin, infoArc);
		}
	}

	public double getCostArc(K idVertexIni, K idVertexFin){
		Vertice<K, V> v1 = tablaHashVertices.get(idVertexIni);
		Vertice<K, V> v2 = tablaHashVertices.get(idVertexFin);
		if(v1 != null && v2 != null){
			Queue<Arco> arcos = tablaHashArcos.get(idVertexIni);
			for(Arco arco : arcos){
				if(arco.darVer2().equals(v2)){
					return arco.darCosto();
				}
			}
		}
		return 0;
	}

	public void setCostArc(K idVertexIni, K idVertexFin, double infoArc){
		Vertice<K, V> v1 = tablaHashVertices.get(idVertexIni);
		Vertice<K, V> v2 = tablaHashVertices.get(idVertexFin);
		if(v1 != null && v2 != null){
			Queue<Arco> arcos1 = tablaHashArcos.get(idVertexIni);
			for (Arco arco : arcos1) {
				if((arco.darVer2().darIdVertice().equals(idVertexFin))){
					arco.cambiarCosto(infoArc);
				}
			}
			Queue<Arco> arcos2 = tablaHashArcos.get(idVertexFin);
			for (Arco arco : arcos2) {
				if((arco.darVer2().darIdVertice().equals(idVertexIni))){
					arco.cambiarCosto(infoArc);
				}
			}
		}
	}

	public Iterator<K> adj(K idVertex){
		Queue<Arco> arcos1 = tablaHashArcos.get(idVertex);
		Queue<K> colaArcos = new Queue<K>();
		if(arcos1 != null) {
			for (Arco arcos : arcos1) {
				colaArcos.enqueue((K) arcos.darVer2().darIdVertice());
			}
		}

		return colaArcos.iterator();
	}

	public void unchecked() {
		for (K b : tablaHashVertices.keys()) {
			tablaHashVertices.get(b).cambiarMarcado(false);
		}

	}

	private void dfs( K pV) {

		tablaHashVertices.get(pV).cambiarMarcado(true);
		Iterator<K> f = adj(pV);
		while(f.hasNext()) {
			Vertice<K,V> w =tablaHashVertices.get(f.next());
			if(!w.estaMarcado()) {
				dfs(w.darIdVertice());

			}
		}

	}
	public int cc () {
		unchecked();
		int cont = 0;

		for (K b : tablaHashVertices.keys()) {
			if(!tablaHashVertices.get(b).estaMarcado()) {
				dfs(b);
				cont++;
			}
		}
		return cont;
	}
	public Iterable<K> getCC (K idVertex){
		unchecked();
		Queue<K> queue = new Queue<K>();
		dfs(idVertex);
		for (K k : tablaHashVertices.keys()) {
			if(tablaHashVertices.get(k).estaMarcado() && k != idVertex) {
				queue.enqueue(k);
			}
		}
		return queue;
	}

}