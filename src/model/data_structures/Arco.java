package model.data_structures;

public class Arco <K,V>{

	private double costo;
	private Vertice<K,V> ver1;
	private Vertice<K,V> ver2;

	public Arco(Vertice<K,V> pVer1, Vertice<K,V> pVer2, double pCosto){
		ver1 = pVer1;
		ver2 = pVer2;
		costo = pCosto;
	}
	
	public Vertice<K,V> darVer1() {
		return ver1;
	}
	
	public Vertice<K,V> darVer2() {
		return ver2;
	}
	
	public double darCosto() {
		return costo;
	}
	
	public void cambiarCosto(double pCos) {
		costo = pCos;
	}
}
