package model.data_structures;

public class InfoVertice {

	private double longitudVer;
	private double latitudVer;
	private int idZonaUber;
	
	public InfoVertice(double pLong, double pLati, int pZona) {
		idZonaUber=pZona;
		latitudVer=pLati;
		longitudVer=pLong;
	}
	
	public double darLongitud() {
		return longitudVer;
	}
	
	public double darLatitud() {
		return latitudVer;
	}
	
	public int darZonaUber() {
		return idZonaUber;
	}
}
