package model.logic;

import java.util.Comparator;

public class TravelTime implements Comparable<TravelTime>{
	
	
	private int sourceId;
	
	private int dstId;
	
	private int month;
	
	private double meanTravelTime;
	
	private double standardDeviationTravelTime;

	
	
	public TravelTime(int a, int b, int c, double d, double e) {
		sourceId=a;
		dstId=b;
		month=c;
		meanTravelTime=d;
		standardDeviationTravelTime=e;
	
	}


	public int getSourceId() {
		return sourceId;
	}


	public int getDstId() {
		return dstId;
	}


	public int getDow() {
		return month;
	}


	public double getMeanTravelTime() {
		return meanTravelTime;
	}


	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}

	public static class comparatorInicial implements Comparator <TravelTime>
	{	@Override
	public int compare(TravelTime arg1, TravelTime arg2) {
		int retorno = (int) (arg1.getMeanTravelTime() - arg2.getMeanTravelTime());
        
		
		
			return retorno;
		

	}}

	@Override
	public int compareTo(TravelTime o) {
		// TODO Auto-generated method stub
		return 0;
	}


	
}
