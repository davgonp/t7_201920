package model.logic;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.InfoWindowOptions;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.*;

public class Maps extends MapView {

	// Objeto Google Maps
	private Map map;

	//Coordenadas del camino a mostrar (secuencia de localizaciones (Lat, Long))
	private LatLng[] locations = {new LatLng(4.597714,-74.094723), new LatLng(4.597714,-74.062707),new LatLng(4.621360, -74.062707),new LatLng(4.621360,-74.094723)  }; //Coordenadas de los vertices inicio, intermedio y fin.		

	private CircleOptions settingsCircle;
	private  PolylineOptions settingsLine;
	private GrafNoDirigido grafo;
	
	public Maps(GrafNoDirigido pGrafo)
	{	
		grafo=pGrafo;
		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#FF0000");
		settingsCircle.setRadius(10);
		settingsCircle.setFillOpacity(0.35);

		settingsLine=new PolylineOptions();
		settingsLine.setGeodesic(true);
		settingsLine.setStrokeColor("#FF0000");
		settingsLine.setStrokeOpacity(1.0);
		settingsLine.setStrokeWeight(2.0);
		
		setOnMapReadyHandler( new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status)
			{
				if ( status == MapStatus.MAP_STATUS_OK )
				{
					map = getMap();

					// marker at the start of path
					Marker startPath = new Marker(map);
					startPath.setPosition(locations[0]);

					// Configuracion de localizaciones intermedias del path (circulos)
					CircleOptions middleLocOpt= new CircleOptions(); 
					middleLocOpt.setFillColor("#00FF00");  // color de relleno
					middleLocOpt.setFillOpacity(0.5);
					middleLocOpt.setStrokeWeight(1.0);

					// Localizacion intermedia 1
					Circle middleLoc1 = new Circle(map);
					middleLoc1.setOptions(middleLocOpt);
					middleLoc1.setCenter(locations[1]); 
					middleLoc1.setRadius(20); //Radio del circulo




					// Localizacion intermedia 2 (Uniandes)
					Circle middleLoc2 = new Circle(map);
					middleLoc2.setOptions(middleLocOpt);
					middleLoc2.setCenter(locations[2]); 
					middleLoc2.setRadius(25); //Radio del circulo

					// marker at the end of path
					Marker endPath = new Marker(map);
					endPath.setPosition(locations[3]);

					//Configuracion de la linea del camino
					PolylineOptions pathOpt = new PolylineOptions();
					pathOpt.setStrokeColor("#0000FF");	  // color de linea	
					pathOpt.setStrokeOpacity(1.75);
					pathOpt.setStrokeWeight(1.5);
					pathOpt.setGeodesic(false);

					// Linea del camino
					Polyline path = new Polyline(map); 														
					path.setOptions(pathOpt); 
					path.setPath(locations);

					generarArcos();
					
					initMap( map );
				}
			}

		} );
		

	}

	public void initMap(Map map)
	{
		MapOptions mapOptions = new MapOptions();
		MapTypeControlOptions controlOptions = new MapTypeControlOptions();
		controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
		mapOptions.setMapTypeControlOptions(controlOptions);

		map.setOptions(mapOptions);
		map.setCenter(locations[2]);
		map.setZoom(14.0);

	}

	public void initFrame(String titulo)
	{
		JFrame frame = new JFrame(titulo);
		frame.setSize(800, 800);
		frame.add(this, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	public void generarArcos()
	{
		Iterable<Integer> llaves = grafo.tablaHashVertices.keys();
		for (Integer i : llaves) {
			Queue<Arco> cola = (Queue<Arco>) grafo.tablaHashArcos.get(i);
			if(cola!=null) {
				for (Arco arco : cola) {
					InfoVertice infoVer1 = (InfoVertice) arco.darVer2().darInfoVer();
					InfoVertice infoVer2 = (InfoVertice) arco.darVer1().darInfoVer();
					if(infoVer1.darLatitud()>4.597714 && infoVer1.darLatitud()< 4.621360 &&
							infoVer2.darLatitud()>4.597714 && infoVer2.darLatitud()< 4.621360
							&& infoVer1.darLongitud()>-74.094723 && infoVer1.darLongitud()<-74.062707
							&& infoVer2.darLongitud()>-74.0947232 && infoVer2.darLongitud()<-74.062707)
						generateSimplePath(new LatLng(infoVer1.darLatitud(), infoVer1.darLongitud()), new LatLng(infoVer2.darLatitud(), infoVer2.darLongitud()));
				}
			}
			Vertice ver = (Vertice) grafo.tablaHashVertices.get(i);
			InfoVertice infoVer3 = (InfoVertice) ver.darInfoVer();
			if(infoVer3.darLatitud()>4.597714 && infoVer3.darLatitud()< 4.621360
					&& infoVer3.darLongitud()>-74.094723 && infoVer3.darLongitud()<-74.062707)
				generateArea(new LatLng(infoVer3.darLatitud(), infoVer3.darLongitud()));

		}

	}

	public void GenerateLine(LatLng... path)
	{
		Polyline polyline = new Polyline(map);
		polyline.setPath(path);
	}
	public void generateArea(LatLng center)
	{
		Circle circle = new Circle(map);
		circle.setCenter(center);
		circle.setRadius(0.0001);
		circle.setVisible(true);
		circle.setOptions(settingsCircle);
	}
	public void generateSimplePath(LatLng start,LatLng end)
	{
		LatLng[] path = {start,end};
		Polyline polyline = new Polyline(map);
		polyline.setPath(path);
	}
	


	public CircleOptions getSettingsCircle() {
		return settingsCircle;
	}

	public void setSettingsCircle(CircleOptions settingsCircle) {
		this.settingsCircle = settingsCircle;
	}

}
