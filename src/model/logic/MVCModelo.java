package model.logic;


import java.awt.AlphaComposite;
import java.io.*;

import java.util.ArrayList;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.google.gson.*;
import model.data_structures.*;




/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */



	private GrafNoDirigido<Integer,InfoVertice> grafo;


	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		grafo=new GrafNoDirigido<Integer,InfoVertice>();
	}

   public GrafNoDirigido darGrafo() {
	   return grafo;
   }


	public void cargar() {
		//		int rta =0;
		//		int rta2=0;
		//		int rta3=0;


		try 
		{ 
			//			Gson gson=new Gson();
			//			BufferedReader br = new BufferedReader(new FileReader("./data/bogota_cadastral.json")); 
			//			geometry[] geo= gson.fromJson(br, geometry[].class)	;	
			//			properties[] prop=  gson.fromJson(br,properties[].class); 
			//			rta=geo.length;	
			//			for (int j =0; j<rta;j++)
			//			{
			//				
			//
			//			}
			//
			//			for (int i =1; i<3;i++)
			//			{	
			//				reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+i+"-All-MonthlyAggregate.csv"));
			//				String[] nextLine=reader.readNext();
			//
			//
			//				while ((nextLine = reader.readNext()) != null) 
			//				{
			//					TravelTime elem = new TravelTime(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
			//							Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
			//							Double.parseDouble(nextLine[4]));
			//					
			//					viajes.add(elem);
			//					rta2++;
			//
			//				}
			//			}

			File f = new File("./data/bogota_vertices.txt");
			FileReader fr= new FileReader(f);
			BufferedReader bf= new BufferedReader(fr);
			bf.readLine();
			while (bf.readLine()!=null){  
				String word= bf.readLine();
				String[] datos = word.split(";");
				InfoVertice infoV = new InfoVertice(Double.parseDouble(datos[1]),Double.parseDouble(datos[2]),Integer.parseInt(datos[3]));
				grafo.addVertex(Integer.parseInt(datos[0]), infoV);
			}
			bf.close();


			File f2 = new File("./data/bogota_arcos.txt");
			FileReader fr2= new FileReader(f2);
			BufferedReader bf2= new BufferedReader(fr2);

			while (bf2.readLine()!=null){  
				String word= bf2.readLine();
				String[] datos = word.split(" ");
				if(grafo.getInfoVertex(Integer.parseInt(datos[0])) !=null){

					double startLat=grafo.getInfoVertex(Integer.parseInt(datos[0])).darLatitud();
					double startLong=grafo.getInfoVertex(Integer.parseInt(datos[0])).darLongitud();

					for(int i =1; i< datos.length;i++) {
						if(grafo.getInfoVertex(i)!=null) {


							double endLat=grafo.getInfoVertex(i).darLatitud();
							double endLong=grafo.getInfoVertex(i).darLongitud();


							double dLat  = Math.toRadians((endLat - startLat));
							double dLong = Math.toRadians((endLong - startLong));

							startLat = Math.toRadians(startLat);
							endLat   = Math.toRadians(endLat);

							double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(dLong / 2), 2);
							double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

							double d= 6371 * c;

							grafo.addEdge(Integer.parseInt(datos[0]), Integer.parseInt(datos[i]), d);
						}
					}
				}


			}
			bf2.close();
			
			FileWriter fNew= new FileWriter("./data/JSONmallaVial.json");
			Gson json1 = new GsonBuilder().create();
			json1.toJson(grafo,fNew);
			fNew.flush();
			fNew.close();
			
			
		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}

		System.out.println("El número de vertices es: "+grafo.V()+ " y el número de arcos es: "+grafo.E());
	}
	
	public void cargarJSON() {
		Gson gson=new Gson();
		grafo = new GrafNoDirigido<Integer, InfoVertice>();
		try (BufferedReader br = new BufferedReader(new FileReader("./data/JSONmallaVial.json"))){
			String linea = br.readLine();
			grafo = gson.fromJson(linea, GrafNoDirigido.class);
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("El número de vertices es: " + grafo.V() + " y el número de arcos es: " + grafo.E());
	}
	
	public int ccGrafo() {
		return grafo.cc();
	}




}