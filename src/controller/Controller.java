package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.*;
import model.logic.MVCModelo;
import model.logic.Maps;
import model.logic.TravelTime;

import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;


		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Cargando datos");
				modelo.cargar();
				break;

			case 2:
				System.out.println("Cantidad componentes conectadas");
				System.out.println("El número de componentes conectadas es " + modelo.ccGrafo());
				break;

			case 3:
				System.out.println("Cargando archivo");
				modelo.cargarJSON();
				break;

			case 4:
				System.out.println("Graficando");
				Maps maps = new Maps(modelo.darGrafo());
				maps.initFrame("Requerimiento");
				break;


			case 5: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;


			default: lector.close();
			System.out.println("--------- \n Opcion Invalida !! \n---------");
			break;
			}
		}

	}


}
